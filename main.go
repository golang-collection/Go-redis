package main

import (
	"GO-redis/newProto"
	"GO-redis/server"
	"fmt"
	"github.com/micro/go-micro/v2"
	"time"
)

/**
* @Author: super
* @Date: 2020-08-17 16:59
* @Description:
**/

func main() {
	service := micro.NewService(
		micro.Name("go.micro.service.redis"),
		micro.RegisterTTL(time.Second*10),
		micro.RegisterInterval(time.Second*5),
	)
	service.Init()

	// 注册处理器
	newProto.RegisterRedisOperationHandler(service.Server(), new(server.RedisStruct))

	// 运行服务
	if err := service.Run(); err != nil {
		fmt.Println(err)
	}
}
